const SellerCon = require('../controllers/SellerCon')
const router = require('express').Router()

router.post('/register', SellerCon.postRegister)

router.post('/login', SellerCon.postLogin)

router.get('/', SellerCon.getSeller)

module.exports  = router