const ProductCon = require('../controllers/productCon')
const { authenticate, authorize } = require('../middlewares/auth')
const router = require('express').Router()

router.post('/', authenticate, ProductCon.postProduct);

router.get('/', authenticate, ProductCon.getProduct);

router.put('/:id', authenticate, ProductCon.putProduct);

router.delete('/:id', authenticate, ProductCon.deleteProduct);

module.exports = router