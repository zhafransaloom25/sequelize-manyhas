const router = require('express').Router()
const ProductRou = require('./ProductRou')
const SellerRou = require('./SellerRou')

router.use('/product', ProductRou);
router.use('/seller', SellerRou)

module.exports = router