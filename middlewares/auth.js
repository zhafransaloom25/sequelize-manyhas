const { Product, Seller } = require('../models');
const { verifyToken } = require('../helpers/jwt');

function authenticate(req, res, next) {
    const { access_token } = req.headers;
    // console.log('token ', req.headers);

    if (access_token) {
        // token di verify ke helpers/jwt
        const decoded = verifyToken(access_token)
        // console.log('verify token :', decoded);
        Seller.findOne({
            // email dijadikan landasan
            // agar dicek auth akun seseorang
            // agar tidak ada email yang ganda
            where: {
                email: decoded.email
            }
        })

            .then((seller) => {
                // 
                // console.log('verify seller :', seller);
                if (!seller) {
                    res.status(401).json({ msg: `Invalid access token!` })
                } else {
                    res.seller = { id: seller.id }  
                    next()
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({ msg: `Internal server is an error! /middlewares` })
            });
    } else {
        res.status(401).json({ msg: `invalid access token! ` })
    }
}

function authorize(req, res, next) {
    const id = +req.params.id
    console.log(id);
    Product.findOne({
        where: { id }
    })
        .then((data) => {
            if (data) {
                const valid = req.seller.id === Product.sellerid
                if (valid) {
                    next()
                } else {
                    res.status(400).json(`Unauthorized /middlewares`)
                }
            } else {
                res.status(400).json(`Unauthorized /middlewares`)
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                msg: `internal server is an error`
            })
        });
}

module.exports = {
    authenticate, authorize
};