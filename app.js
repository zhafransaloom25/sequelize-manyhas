const express = require('express')
const app = express()
const port = 5000
const router = require('./routes')

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(router)

app.get('/', (req, res) => res.send('Hello Mba Aca!, ini Tugas Sudah Dikerjain nih'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
