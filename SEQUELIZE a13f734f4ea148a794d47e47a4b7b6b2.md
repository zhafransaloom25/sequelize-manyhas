# SEQUELIZE

**INSTALL LIB**

install node.js

`npm init`

---

install Express

`npm install express`

---

install Sequelize

`npm install --save sequelize`

---

install sequelize-cli

`npm install -D sequelize-cli`NPX

---

install mysql

`npm install mysql2`

---

**Authentication**

install jsonwebtoken

`npm install jsonwebtoken`

install to hash passwords

`npm install bcrypt`

install nodemon 

`npm install -g nodemon`

---

add file in folder

.gitignore

⇒ node_modules

---

add file app.js

```jsx
const express = require('express')
const app = express()
const port = 5000
const router = require('./routes')

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(router)

app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
```

---

mengenerate 4 folder sequelize

`npx sequelize-cli init`

- config
    - config.json
- migrations
- models
    - index.js
- node_modules
- seeders
- gitignore
- app.js
- package-lock.json
- package.json

penjelasan setiap folders

| config | connect with database |
| --- | --- |
| models | all models in project |
| migratios | all migration files |
| seeders | all seed file |

---

edit config ⇒ config.js

```json
"development": {
"username": "root",
"password": "",
"database": "Sequelize_hasmany",
"host": "127.0.0.1",
"dialect": "mysql"
},
```

Create database

`npx sequelize-cli db:create` 

---

table Seller

| email | string |
| --- | --- |
| password | string |
| no_tlpn | integer |
| no_rek | integer |

`npx sequelize-cli model:generate --name Seller --attributes email:string,password:string,no_tlpn:integer,no_rek:integer`

---

table Product

| name | stirng |
| --- | --- |
| category | string |
| stock | integer |
| price | integer |
| color | string |
| size | string |
| sellerid | integer |

<aside>
💡 sellerid akan relasi dengan table Seller (sellerid.product == id.Seller)

</aside>

`npx sequelize-cli model:generate --name Product --attributes name:string,category:string,stock:integer,price:integer,color:string,size:string,sellerid:integer`

---

Run Migratio

`npx sequelize-cli db:migrate`

membatalkan perubahan

`npx sequelize-cli db:migrate:undo`

---

menambah folder controllers ⇒ - productCon.js, SellerCon.js

- config
    - config.json
- controllers
    - ProductCon.js
    - SellerCon.js
- **migrations**
- models
    - index.js
- ~~node_modules~~
- seeders
- gitignore
- app.js
- ~~package-lock.json~~
- ~~package.json~~

---

buka file Create-Product di Migration

tambahkan Forigenkey pada sellerid

| name | stirng | primary key 🧡 |
| --- | --- | --- |
| category | string |  |
| stock | integer |  |
| price | integer |  |
| color | string |  |
| size | string |  |
| sellerid | integer | forigen key 💜 |

add script to migrations ⇒ create-product 

```jsx
sellerid: {
	type: Sequelize.INTEGER,
	allowNull: false,
	Reference: {
		model: {
			tableName : 'Sellers'
		},
		key: 'id'
	},
	onUpdate: 'CASCADE',
	onDelete: 'CASCADE'
},
```

---

add folder routes ⇒ index.js, ProductRou.js, SellerRou.js

- config
    - config.json
- controllers
    - ProductCon.js
    - SellerCon.js
- **migrations**
- models
    - index.js
- ~~node_modules~~
- **routes**
    - index.js
    - ProductRou.js
    - SellerRou.js
- seeders
- gitignore
- app.js
- ~~package-lock.json~~
- ~~package.json~~

index.js

```jsx
const router = require('express').Router()
const ProductRou = require('./ProductRou')
const SellerRou = require('./SellerRou')

router.use('/product', ProductRou);
router.use('/seller', SellerRou)

module.exports = router
```

ProductRou.js

```jsx
const ProductCon = require('../controllers/productCon')
const { authenticate } = require('../middlewares/auth')
const router = require('express').Router()

router.post('/', authenticate, ProductCon.postProduct);

router.get('/', authenticate, ProductCon.getProduct);

router.put('/:id', authenticate, ProductCon.putProduct);

router.delete('/:id', authenticate, ProductCon.deleteProduct);

module.exports = router
```

SellerRou.js

```jsx
const SellerCon = require('../controllers/SellerCon')
const router = require('express').Router()

router.post('/register', SellerCon.postRegister)

router.post('/login', SellerCon.postLogin)

router.get('/', SellerCon.getSeller)

module.exports  = router
```

---

membuat folder Helpers ⇒ jwt.js, bcrypt.js

- config
    - config.json
- controllers
    - ProductCon.js
    - SellerCon.js
- helpers
    - bcrypt.js
    - jwt.js
- **migrations**
- models
    - index.js
- ~~node_modules~~
- **routes**
    - index.js
    - ProductRou.js
    - SellerRou.js
- seeders
- gitignore
- app.js
- ~~package-lock.json~~
- ~~package.json~~

open helpers ⇒ jwt.js

untuk TokenSecret bisa dibuat manual
disini sy menggunakan TS menjadi dasar token

```jsx
const jwt = require('jsonwebtoken')
const TS = "1029384756"

function generateToken(username) {
	  const token = jwt.sign(username, TS);
    return token
    // membuat token 
}

function verifyToken(token) {
    const verify = jwt.verify(token, TS);
    return verify
		// verify token yang telah dibuat
// padahal kenapa tidak di 
}

module.exports = {
    generateToken,verifyToken
} //jangan lupa di export agar kelas lain bisa paket
```

---

open helpers ⇒ bcrypt.js

password akan diubah dari enkripsi dari algoritma bcrypt

```jsx
const bcrypt = require ('bcrypt')

function hashPassword(password){
	const salt = bcrypt.genSaltSync(10)
// salt menjadikan algoritma enkripsy password
	const hash = bcrypt.hashSync(password, salt)
// password dimasukan dan ubah menggunakan salt
	return hash
}

function comparePassword(password, hash){
	const compare = bcrypt.compareSync (password, hash)
// data password digabung dengan function hash 
	return compare
}

module.exports = {
	hashPassword,
	comparePassoword
}
```

---

membuat folder middlewares ⇒ auth.js

- config
    - config.json
- controllers
    - ProductCon.js
    - SellerCon.js
- helpers
    - bcrypt.js
    - jwt.js
- middlewares
    - auth.js
- **migrations**
- models
    - index.js
- ~~node_modules~~
- **routes**
    - index.js
    - ProductRou.js
    - SellerRou.js
- seeders
- gitignore
- app.js
- ~~package-lock.json~~
- ~~package.json~~

auth.js

```jsx
//import function
const { Product, Seller } = require('../models');
const { verifyToken } = require('../helpers/jwt');

module.exports = {
    authenticate, authorize
};
```

```jsx
//membuat function authenticate menilai token
function authenticate(req, res, next) {
    const { access_token } = req.headers;
    console.log('token ', req.headers);
}
if (access_token) {
	 const decoded = verifyToken(access_token)
   // token diolah function verifytoken di helpers/jwt.js
     
console.log('verify token :', decoded);
	Seller.findOne({ //seller memiliki banyak model kemudian di pilih satu komponen
  where: {
  email: decoded.email
//email menjadi landasan untuk diverify yang ada di seller
  }
//nilai Seller berdasarkan email sudah di verifytoken
  })
.then((seller) => { 
	console.log('verify seller : ', seller);

	if (seller) {
	res.seller = { 
		id: seller.id
	}
	next()
} 
else {
res.status(401).json({
msg: `invalid acces token ! /middlewares`
	 	})
	}
}).catch((err) => {
		console.log(err);
    res.status(500).json({ msg: `Internal server is an error! /middlewares` })
});
    
} else {
        res.status(401).json({ msg: `invalid access token! ` })
    }
}
```

```jsx
function authorize(req, res, next) {
    const id = +req.params.id
// menampilkan id yang di parameter product
    Product.findOne({//product memiliki banyak model kemudian di pilih satu komponen
        where: { id }
//id menjadi landasan untuk menampilkan yang ada di product
    })
.then((data) => {
if (data) {
// menampilkan isi data product
const valid = req.seller.id === Product.sellerid
// req sellerid bernilai sama dengan id seller
	if (valid) {
  next()
  } else {
  res.status(400).json(`Unauthorized /middlewares`)
  }
 } else {
 res.status(400).json(`Unauthorized /middlewares`)
 }
 })
 .catch((err) => {
 console.log(err);
 res.status(500).json({
 msg: `internal server is an error`
	 })
 });
}
```

---

**logic sellerCon.js**

controllers ⇒ SellerCon.js

```jsx
// import function
const { hashPassword } = require('../helpers/bcrypt');
const { Product, Seller } = require('../models');
const { generateToken } = require('../helpers/jwt');
```

```jsx
class SellerCon {
	static postRegister (req,res){}
	static postLogin (req, res){}
	static getSeller (req,res){}
}

module.exports = SellerCon;
```

```jsx
static postRegister(req, res){
const {email,password,no_tlpn,no_rek}=req.body
const passordhash = hashPassword(password)
Seller.create({
	email, password:passwordhash, no_tlpn, no_rek
})
.then((data)=>{
	let seller = {
	email:data.email,
	password:data.password,
	no_tlpn:data.no_tlpn,
	no_rek: data.no_rek
	}
	res.status(201).json({
		msg: `Seller berhasil ditambah`, seller
	})
}).catch((err) =>{
		console.log(err);
		res.status(500).json({
			msg: `Internal server is an error!`
		})
	})
}
```

```jsx
static postLogin(req, res){
	const {email, password}=req.body;
	Seller.findOne({
		where:{email:email}
	})
	.then((data) => {
	if(data){
		console.log(data);
		let checkPassword = comparePassword(password, data.password)
    let username = {
	    id: data.id,
	    email: data.email
    }
		if(checkPassword){
		const access_token = generateToken(username)
    res.status(200).json({access_token})
		}else{
		res.status(401).json({msg: `Invalid email/password controllers`})
		}
	}
	else{
	res.status(401).json({msg: `Invalid email/password controllers`})
	}
})
.catch((err) => {
	console.log(err);
	res.status(500).json({
		msg: `Internal server is an error!`
	})
})
}
```

```jsx
static getSeller(req, res){
	Seller.findAll({
	include:{
	model: Product,
	as: "daftar product"
	}
})
	.then((data) => {
	if(data) {
		res.status(200).json({
		msg: data
	})
	}else{
	res.status(500).json({
	msg: `Datanya gaada!!`
	})
}
}).catch((err) => {
	console.log(err)
	res.status(500).json({
	msg: `internal server is an error`
})
})
}
```

---

**logic productCon.js**

```jsx
// import
const { Product, Seller } = require('../models')
```

```jsx
class ProductCon {
	static postProduct   (req,res){}
	static putProduct    (req,res){}
	static getProduct    (req,res){}
	static deleteProduct (req, res){}
}

module.exports = ProductCon;
```

```jsx
static postProduct(req, res) {
        const {
            name,
            category,
            price,
            size,
            color,
            // menampilkan data akan di requies body
            // sellerid tidak dimasukan
            // get sellerid berasal dari id seller 
        } = req.body

        //data Product dimasukan kedalam database
        Product.create({
            name,
            category,
            price,
            size,
            color,
            sellerid : res.Seller.id
            // value sellerid get dari seller id
        })
				.then((data) => {
				res.status(201).json({
				msg: `Product successfully create ${data}`
	        })
        }).catch((err) => {
           console.log(err);
           res.status(500).json({
           msg: `internal server is an error!`
	         })
        });
    }
```

```jsx
static getProduct(req, res) {
        Product.findAll()
        .then((data) => {
        
				if (data) {
        res.status(200).json({
        msg: data
        })
        
				} else {
        res.status(500).json({
        msg: `Datanya gaada!!`
	        })
	       }
       })
			.catch((err) => {
      console.log(err);
      res.status(500).json({
      msg: `internal server is an error!`
      })
     });
    }
```

```jsx
static putProduct(req, res) {
        const id = req.params.id;
        const {
            name,
            category,
            stock,
            price,
            color,
            size
        } = req.body;

        Product.update({
            name,
            category,
            stock,
            price,
            color,
            size
        }, {
            where: { id },
            returning: true
        })

        .then(async (data) => {
        const dataProduct = await Product.findOne({
	        where: { id }
          })
          res.status(200).json({
           dataProduct,
           msg: `Seccessfully edited task id: ${id}`
            })
           })
					.catch((err) => {
          console.log(err);
          res.status(500).json({
          msg: `internal server is an error!`
	          })
           });
					}
```

```jsx
static deleteProduct(req, res) {
        const id = req.params.id
        Product.destroy({
            where: { id }
        })
       .then((data) => {
	       if (data) {
	        res.status(200).json({
	          msg: `Product has been deleted id: ${id}`
             })
         } else {
	         res.status(404).json({
           msg: `Product is not found`
          })
         }
        })
			.catch((err) => {
      console.log(err);
      res.status(500).json({
	      msg: `internal server is an error!`
       })
      });
    }
```

---

menambahkan models ⇒ Seller.js, Product.js

- config
    - config.json
- controllers
    - ProductCon.js
    - SellerCon.js
- helpers
    - bcrypt.js
    - jwt.js
- middlewares
    - auth.js
- **migrations**
- models
    - index.js
    - product.js
    - seller.js
- ~~node_modules~~
- **routes**
    - index.js
    - ProductRou.js
    - SellerRou.js
- seeders
- gitignore
- app.js
- ~~package-lock.json~~
- ~~package.json~~

Seller.js

```jsx
static associate(models) {
      // define association here
      //seller punya banyak Product
      this.hasMany(models.Product, {
        as:'Product',
      })
    }
```

```jsx
Seller.init({
    email: {
      type: DataTypes.STRING,
      validasi: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
        isEmail: {
          args: true,
          msg: `Format must be email!`
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        }
        
      }
    },
    no_tlpn: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
      }
    },
    no_rek: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
      }
    },
  }, {
    sequelize,
    modelName: 'Seller',
  });
  return Seller;
```

Product.js

```jsx
static associate(models) {
      // define association here
			// membuat jalan terhubung ke seller
      this.belongsTo(models.Seller, {
        foreignKey: 'sellerid',
      })
```

---

TERIMA KASIH