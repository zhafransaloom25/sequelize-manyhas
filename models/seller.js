'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seller extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //seller punya banyak Product
      this.hasMany(models.Product, {
        as: 'Product',
      })
    }
  };
  Seller.init({
    email: {
      type: DataTypes.STRING,
      validasi: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
        isEmail: {
          args: true,
          msg: `Format must be email!`
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
      }
    },
    no_tlpn: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
      }
    },
    no_rek: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: `Name must be filled!`
        },
      }
    },
  }, {
    sequelize,
    modelName: 'Seller',
  });
  return Seller;
};