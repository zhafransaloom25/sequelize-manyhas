const jwt = require('jsonwebtoken')
const TS = "1029384756"

function generateToken(username) {
    const token = jwt.sign(username, TS);
    return token
}

function verifyToken(token) {
    const verify = jwt.verify(token, TS);
    return verify
}

module.exports = {
    generateToken, verifyToken
}
