const bcrypt = require('bcrypt');

function hashPassword(password) {
    const salt = bcrypt.genSaltSync(10);
    // salt sebagai algoritma enkripso
    const hash = bcrypt.hashSync(password, salt)
    return hash;
}

function comparePassword(password, hash) {
    const compare = bcrypt.compareSync(password, hash)
    return compare;
}

module.exports = {
    hashPassword,
    comparePassword
}
