const { hashPassword, comparePassword } = require('../helpers/bcrypt');
const { generateToken } = require('../helpers/jwt');
const { Product, Seller } = require('../models');

class SellerCon {

    static postRegister(req, res) {
        const {
            email,
            password,
            no_tlpn,
            no_rek
            //!  data yang di input di body
        } = req.body;
        const passwordhash = hashPassword(password)
        //hashPassword dari bcrypt
        console.log(`password : ${passwordhash}`);
        //Seller yang di import di atas digunakan untuk dimasukan ke database
        Seller.create({
            email,
            password: passwordhash,
            no_tlpn,
            no_rek
        })

            .then((data) => {
                let seller = {
                    id: data.id,
                    email: data.email,
                    password: data.password,
                    no_tlpn: data.no_tlpn,
                    no_rek: data.no_rek
                }
                res.status(201).json({
                    msg: `Seller berhasil ditambah`, seller
                })
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `Internal server is an error!`
                })
            });
    }

    static postLogin(req, res) {
        const { email, password } = req.body;
        Seller.findOne({
            where: {
                email: email
            }
        })
            .then((data) => {
                if (data) {
                    console.log(data);
                    let checkPassword = comparePassword(password, data.password)
                    let username = {
                        id: data.id,
                        email: data.email
                    }
                    if (checkPassword) {
                        const access_token = generateToken(username)
                        res.status(200).json({
                            msg: access_token
                        })
                    } else {
                        res.status(401).json({
                            msg: `Invalid email/password controllers`
                        })
                    }
                } else {
                    res.status(401).json({
                        msg: `Invalid email/password controllers`
                    })
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `Internal server is an error!`
                })
            });
    }

    static getSeller(req, res) {
        Seller.findAll({
            include: {
                model: Product,
                as: "Product"
            }
        })
            .then((data) => {
                if (data) {
                    res.status(200).json({
                        msg: data
                    })
                } else {
                    res.status(500).json({
                        msg: `Datanya gaada!!`
                    })
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `internal server is an error!`
                })
            });
    }
}

module.exports = SellerCon;