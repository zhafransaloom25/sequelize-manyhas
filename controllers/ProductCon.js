const { Product, Seller } = require('../models')

class ProductCon {

    static postProduct(req, res) {
        const {
            name,
            category,
            stock,
            price,
            color,
            size
            //! menampilkan data akan di requies body
            //? sellerid tidak dimasukan
            //! get sellerid berasal dari id seller 
        } = req.body

        //data Product dimasukan kedalam database
        Product.create({
            name,
            category,
            stock,
            price,
            color,
            size,
            sellerid: res.seller.id
            // value sellerid get dari seller id
        })

            .then((data) => {
                res.status(201).json({
                    msg: `Product successfully create ${data}`
                })
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `internal server is an error!`
                })
            });
    }

    static getProduct(req, res) {
        Product.findAll()
            .then((data) => {
                if (data) {
                    res.status(200).json({
                        msg: data
                    })
                } else {
                    res.status(500).json({
                        msg: `Datanya gaada!!`
                    })
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `internal server is an error!`
                })
            });
    }

    static putProduct(req, res) {
        const id = req.params.id;
        const {
            name,
            category,
            stock,
            price,
            color,
            size,
        } = req.body;

        Product.update({
            name,
            category,
            stock,
            price,
            color,
            size,
        }, {
            where: { id },
            returning: true
        })

            .then(async (data) => {
                const dataProduct = await Product.findOne({
                    where: { id }
                })
                res.status(200).json({
                    dataProduct,
                    msg: `Seccessfully edited task id: ${id}`
                })
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `internal server is an error!`
                })
            });
    }

    static deleteProduct(req, res) {
        const id = req.params.id

        Product.destroy({
            where: { id }
        })
            .then((data) => {
                if (data) {
                    res.status(200).json({
                        msg: `Product has been deleted id: ${id}`
                    })
                } else {
                    res.status(404).json({
                        msg: `Product is not found`
                    })
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    msg: `internal server is an error!`
                })
            });
    }
}

module.exports = ProductCon;